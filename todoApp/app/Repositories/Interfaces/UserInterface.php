<?php

namespace App\Repositories\Interfaces;

interface UserInterface
{
	public function createUser($userData);
	public function getUser($userId);
	public function getAllUsers();
	public function updateUser($userData, $userId);
	public function deleteUser($userId);
}