<?php

namespace App\Repositories;

use App\Repositories\Interfaces\TaskInterface;
use App\Models\Task;

class TaskRepository implements TaskInterface
{

	protected $task;

	public function __construct(Task $task)
	{
		$this->task = $task;
	}

	public function createTaskFromUser($taskData)
	{
		$this->task->fill($taskData);
		$saved = $this->task->save();
		if ($saved) {
			return ['saved' => true, 'task' => $this->task];
		}
		return ['saved' => false, 'task' => []];
	}

	public function getTaskFromUser($userId, $taskId)
	{
		$task = $this->task->where('user_id',  $userId)->where('id',$taskId)->get();
		if ($task->count()) {
			return ['found' => true, 'task' => $task];
		}
		return ['found' => false, 'task' => []];
	}

	public function updateTaskFromUser($taskData, $userId, $taskId)
	{
		$task = $this->task->where('user_id', '=', $userId)->where('id',$taskId)->first();
		if ($task) {
			$task->fill($taskData);
			$updated = $task->update();
			if ($updated) {
				return ['updated' => true, 'task' => $task];
			}
		}
		return ['updated' => false, 'task' => []];
	}

	public function deleteTaskFromUser($userId, $taskId)
	{
		$task = $this->task->where('user_id', $userId)->where('id', $taskId);
		if ($task->count()) {
			$task->delete();
			return ['deleted' => true, 'task' => []];
		}
		return ['deleted' => false, 'task' => []];
	}

	public function listAllTasksFromUser($userId, $filters)
	{
		$tasks = $this->task->where('user_id', $userId)->orderBy('created_at', 'desc');

		if (isset($filters['status'])) {
			$tasks = $tasks->where('status', $filters['status']);
		}

		if (isset($filters['created_at'])) {
			$tasks = $tasks->whereDate('created_at', $filters['created_at']);
		}


		if ($tasks) {
			return ['found' => true, 'task' => $tasks->paginate(20)];
		}
		return ['found' => false, 'task' => []];
	}
}