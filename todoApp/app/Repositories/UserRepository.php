<?php

namespace App\Repositories;

use App\Repositories\Interfaces\UserInterface;
use App\User;

class UserRepository implements UserInterface
{

	protected $user;

	public function __construct(User $user)
	{
		$this->user = $user;
	}

	public function createUser($userData)
	{
		$this->user->fill($userData);
		$saved = $this->user->save();

		if ($saved) {
			return ['saved' => true, 'user' => $this->user];
		}
		return ['saved' => false, 'user' => []];
	}

	public function getUser($id)
	{
		$userFound = $this->user->find($id);
		if ($userFound) {
			return ['found' => true, 'user' => $userFound];
		}
		return ['found' => false, 'user' => []];
	}

	public function getAllUsers()
	{
		$users = $this->user->orderBy('created_at', 'desc')->paginate(20);
		if ($users) {
			return ['found' => true, 'users' => $users];
		}
		return ['found' => false, 'users' => []];
	}

	public function updateUser($userData, $userId)
	{
		$user = $this->user->find($userId);
		if ($user) {

			$user->fill($userData);
			$updated = $user->save();

			if ($updated) {
				return ['updated' => true, 'user' => $user];
			}
		}

		return ['updated' => false, 'user' => []];
	}

	public function deleteUser($id)
	{
		$userFound = $this->user->find($id);
		if ($userFound) {
			$userFound->delete();
			return ['deleted' => true, 'user' => []];
		}
		return ['deleted' => false, 'user' => []];
	}
}