<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Requests\CreateTaskRequest;
use App\Http\Requests\UpdateTaskRequest;
use App\Repositories\Interfaces\TaskInterface;
use Illuminate\Support\Facades\Response;
use Illuminate\Http\Request;
use App\Http\Controllers\BaseController;

class TaskController extends BaseController
{

	protected $taskRepository;

	public function __construct(TaskInterface $taskRepository)
	{
		$this->taskRepository = $taskRepository;
	}

	public function createTask(CreateTaskRequest $request)
	{
		$data = $request->all();
		$data['user_id'] = $request->id;

		$task = $this->taskRepository->createTaskFromUser($data);
		if ($task['saved']) {
			return $this->response_data('Task created successfully', $task['task'], 200);
		}

		return $this->response_data('Error saving task', $task['task'], 500);
	}

	public function listTasks(Request $request)
	{
		$filters = ['status', 'created_at'];

		if ($request->get('status')) {
			$filters['status'] = $request->get('status');
		}

		if ($request->get('created_at')) {
			$filters['created_at'] = $request->get('created_at');
		}

		$tasks = $this->taskRepository->listAllTasksFromUser($request->id, $filters);
		if ($tasks['found']) {
			return $this->response_data('', $tasks['task'], 200);
		}
		return $this->response_data('Task not found', $tasks['task'], 404);
	}

	public function getTask(Request $request)
	{
		$task = $this->taskRepository->getTaskFromUser($request->id, $request->taskId);
		if ($task['found']) {
			return $this->response_data('', $task['task'], 200);
		}
		return $this->response_data('Task not found', $task['task'], 404);
	}

	public function deleteTask(Request $request)
	{
		$task = $this->taskRepository->deleteTaskFromUser($request->id, $request->taskId);
		if ($task['deleted']) {
			return $this->response_data('Task deleted successfully', $task['task'], 200);
		}
		return $this->response_data('Task not found', $task['task'], 404);
	}

	public function updateTask(UpdateTaskRequest $request)
	{
		$data = $request->all();
		$data['user_id'] = $request->id;
		$task = $this->taskRepository->updateTaskFromUser($data, $request->id, $request->taskId);
		if ($task['updated']) {
			return $this->response_data('Task updated successfully', $task['task'], 200);
		}

		return $this->response_data('Task not found', $task['task'], 404);
	}
}
