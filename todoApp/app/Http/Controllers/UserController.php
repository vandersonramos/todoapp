<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateUserRequest;
use App\Http\Requests\UpdateUserRequest;
use App\Repositories\Interfaces\UserInterface;
use Illuminate\Http\Request;
use App\Http\Controllers\BaseController;
use Illuminate\Support\Facades\Hash;

class UserController extends BaseController
{

	protected $userRepository;

	public function __construct(UserInterface $userRepository)
	{
		$this->userRepository = $userRepository;
	}

	public function createUser(CreateUserRequest $request)
	{
		$data = $request->all();
		$data['password'] = Hash::make($data['password']);
		$image = $request->file('avatar');

		if ($image) {
			$data['avatar'] = time().'.'.$image->getClientOriginalExtension();
			$destinationPath = public_path('/images');
			$image->move($destinationPath, $data['avatar']);
		}

		$user = $this->userRepository->createUser($data);
		if ($user['saved']) {
			return $this->response_data('User created successfully', $user['user'], 200);
		}

		return $this->response_data('Error saving user', $user['user'], 500);
	}

	public function deleteUser(Request $request)
	{
		$user = $this->userRepository->deleteUser($request->id);
		if ($user['deleted']) {
			return $this->response_data('User deleted successfully', $user['user'], 200);
		}
		return $this->response_data('User not found', $user['user'], 404);
	}

	public function listUser(Request $request)
	{
		$user = $this->userRepository->getUser($request->id);
		if ($user['found']) {
			return $this->response_data('', $user['user'], 200);
		}
		return $this->response_data('User not found', $user['user'], 404);
	}

	public function listAllUsers(Request $request)
	{
		$users = $this->userRepository->getAllUsers();
		if ($users['found']) {
			return $this->response_data('', $users['users'], 200);
		}
		return $this->response_data('Users is empty', $users['users'], 200);
	}

	public function updateUser(UpdateUserRequest $request)
	{
		$data = $request->all();
		$image = $request->file('avatar');

		if (isset($data['password'])) {
			$data['password'] = Hash::make($data['password']);
		}

		if ($image) {
			$data['avatar'] = time().'.'.$image->getClientOriginalExtension();
			$destinationPath = public_path('/images');
			$image->move($destinationPath, $data['avatar']);
		}

		$user = $this->userRepository->updateUser($data, $request->id);
		if ($user['updated']) {
			return $this->response_data('User updated successfully', $user['user'], 200);
		}
		return $this->response_data('User not found', $user['user'], 404);
	}
}
