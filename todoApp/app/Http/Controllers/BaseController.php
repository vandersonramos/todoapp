<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Response;

class BaseController extends Controller
{

	public function response_data($message, $user, $statusCode)
	{
		$result = [
			'message' => $message,
			'data' => $user
		];

		return Response::json($result, $statusCode);
	}
}
