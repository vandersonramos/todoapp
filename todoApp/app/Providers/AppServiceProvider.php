<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('App\Repositories\Interfaces\UserInterface', 'App\Repositories\UserRepository');
        $this->app->bind('App\Repositories\Interfaces\TaskInterface', 'App\Repositories\TaskRepository');
        $this->app->bind('App\Repositories\Interfaces\TaskItemInterface', 'App\Repositories\TaskItemRepository');
        $this->app->bind('App\Repositories\Interfaces\TaskAttachmentInterface', 'App\Repositories\TaskAttachmentRepository');
    }
}
