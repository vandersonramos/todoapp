<?php

namespace App\Models;

class EnumerationStatus
{
	public static $statusList = ['ACTIVATED', 'FINISHED', 'DEACTIVATED'];
}