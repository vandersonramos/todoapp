<?php

namespace App\Repositories;

use App\Repositories\Interfaces\TaskItemInterface;
use App\Models\TaskItem;

class TaskItemRepository implements TaskItemInterface
{

	protected $item;

	public function __construct(TaskItem $item)
	{
		$this->item = $item;
	}

	public function createItemFromTask($taskItemData, $taskId, $userId)
	{

		$tasks = $this->item->join('list', 'list_item.list_id', 'list.id')
				->where('user_id', $userId)
				->where('list_id', $taskId)
				->where('list_item.status', 'ACTIVATED')
				->whereDate('list_item.dueDate', '=', date("Y-m-d"))
				->get(['list_item.*']);

		if ($tasks->count() == 3) {
			$dueDate = (new \DateTime($taskItemData['dueDate']))->format('Y-m-d');
			$now = (new \DateTime())->format('Y-m-d');
			if ($dueDate == $now) {
				return ['dueDateLimited' => true];
			}
		}

		$this->item->fill($taskItemData);
		$saved = $this->item->save();

		if ($saved) {
			return ['saved' => true, 'item' => $this->item];
		}
		return ['saved' => false, 'item' => []];
	}

	public function getItemFromTask($userId, $taskId, $taskItemId)
	{
		$tasks = $this->item->join('list', 'list_item.list_id', 'list.id')
				->where('user_id', $userId)
				->where('list_id', $taskId)
				->where('list_item.id', $taskItemId)
				->get(['list_item.*']);

		if ($tasks->count()) {
			return ['found' => true, 'item' => $tasks];
		}
		return ['found' => false, 'item' => []];
	}

	public function updateItemFromTask($taskItemData, $taskId, $taskItemId)
	{
		$userId = $taskItemData['user_id'];

		$tasks = $this->item->join('list', 'list_item.list_id', 'list.id')
				->where('user_id', $userId)
				->where('list_id', $taskId)
				->where('list_item.status', 'ACTIVATED')
				->where('list_item.id', '!=', $taskItemId)
				->whereDate('list_item.dueDate', '=', date("Y-m-d"))
				->get(['list_item.*']);

		if ($tasks->count() == 3) {
			$dueDate = (new \DateTime($taskItemData['dueDate']))->format('Y-m-d');
			$now = (new \DateTime())->format('Y-m-d');
			if ($dueDate == $now && $taskItemData['status'] == 'ACTIVATED') {
				return ['dueDateLimited' => true];
			}
		}


		$item = $this->item->find($taskItemId);

		if ($item) {
			$updated = $item->update($taskItemData);
			if ($updated) {
				return ['updated' => true, 'item' => $item];
			}
		}
		return ['updated' => false, 'item' => []];
	}

	public function deleteItemFromTask($taskId, $taskItemId)
	{
		$tasks = $this->item->where('list_id', $taskId)->where('id', $taskItemId);

		if ($tasks->count()) {
			$tasks->delete();
			return ['deleted' => true, 'item' => $tasks];
		}
		return ['deleted' => false, 'item' => []];
	}

	public function listAllItemsFromTask($userId, $taskId, $filters)
	{
		$tasks = $this->item->join('list', 'list_item.list_id', 'list.id')
				->where('user_id', $userId)
				->where('list_id', $taskId)
				->orderBy('created_at', 'desc')
				->select(['list_item.*'])
				->paginate(20);

		if (isset($filters['status'])) {
			$tasks = $tasks->where('status', $filters['status']);
		}

		if ($tasks->count()) {
			return ['found' => true, 'items' => $tasks];
		}
		return ['found' => false, 'items' => []];
	}
}