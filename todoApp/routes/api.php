<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['prefix' => 'users'], function () {

	Route::post('', ['uses' => 'UserController@createUser']);
	Route::get('{id}', ['uses' => 'UserController@listUser']);
	Route::get('', ['uses' => 'UserController@listAllUsers']);
	Route::post('{id}', ['uses' => 'UserController@updateUser']);
	Route::delete('{id}', ['uses' => 'UserController@deleteUser']);

	Route::post('{id}/tasks', ['uses' => 'TaskController@createTask']);
	Route::get('{id}/tasks/{taskId}', ['uses' => 'TaskController@getTask']);
	Route::get('{id}/tasks', ['uses' => 'TaskController@listTasks']);
	Route::delete('{id}/tasks/{taskId}', ['uses' => 'TaskController@deleteTask']);
	Route::post('{id}/tasks/{taskId}', ['uses' => 'TaskController@updateTask']);

	Route::post('{id}/tasks/{taskId}/items', ['uses' => 'TaskItemController@createItemFromTask']);
	Route::get('{id}/tasks/{taskId}/items/{itemId}', ['uses' => 'TaskItemController@getItemFromTask']);
	Route::get('{id}/tasks/{taskId}/items/', ['uses' => 'TaskItemController@getAllItemFromTask']);
	Route::delete('{id}/tasks/{taskId}/items/{itemId}', ['uses' => 'TaskItemController@deleteItemFromTask']);
	Route::put('{id}/tasks/{taskId}/items/{itemId}', ['uses' => 'TaskItemController@updateItemFromTask']);

	Route::post('{id}/tasks/{taskId}/attachments', ['uses' => 'TaskAttachmentController@createAttachmentFromTask']);
	Route::get('{id}/tasks/{taskId}/attachments', ['uses' => 'TaskAttachmentController@listAttachmentsFromTask']);
	Route::delete('{id}/tasks/{taskId}/attachments/{attachmentId}', ['uses' => 'TaskAttachmentController@deleteAttachmentFromTask']);


});
