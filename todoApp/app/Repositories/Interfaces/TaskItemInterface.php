<?php

namespace App\Repositories\Interfaces;

interface TaskItemInterface
{
	public function createItemFromTask($taskItemData, $taskId, $userId);
	public function getItemFromTask($userId, $taskId, $taskItemId);
	public function updateItemFromTask($taskItemData, $taskId, $taskItemId);
	public function deleteItemFromTask($taskId, $taskItemId);
	public function listAllItemsFromTask($userId, $taskId, $filters);
}