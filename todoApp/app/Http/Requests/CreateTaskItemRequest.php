<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateTaskItemRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required',
            'status' => 'required|in:ACTIVATED,FINISHED,DEACTIVATED',
            'dueDate' => 'required|date|date_format:"Y-m-d H:i:s"|after:'.date("Y-m-d H:i:s", strtotime('-1 days')),
        ];
    }
}
