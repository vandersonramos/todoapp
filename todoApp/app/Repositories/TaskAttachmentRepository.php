<?php

namespace App\Repositories;

use App\Repositories\Interfaces\TaskAttachmentInterface;
use App\Models\Attachment;

class TaskAttachmentRepository implements TaskAttachmentInterface
{

	protected $attachment;

	public function __construct(Attachment $attachment)
	{
		$this->attachment = $attachment;
	}

	public function createAttachmentFromTask($file)
	{
		$this->attachment->fill($file);
		$saved = $this->attachment->save();

		if ($saved) {
			return ['saved' => true, 'attachment' => $this->attachment];
		}
		return ['saved' => false, 'attachment' => []];
	}

	public function listAttachmentsFromTask($taskId)
	{

		$attachments = $this->attachment->where('list_id', $taskId)->orderBy('created_at', 'desc')->paginate(20);
		if ($attachments->count()) {
			return ['found' => true, 'attachments' => $attachments];
		}
		return ['found' => false, 'attachments' => []];
	}

	public function deleteAttachmentFromTask($fileId)
	{
		$attachmentFound = $this->attachment->find($fileId);
		if ($attachmentFound) {
			$attachmentFound->delete();
			return ['deleted' => true, 'attachment' => []];
		}
		return ['deleted' => false, 'attachment' => []];
	}
}