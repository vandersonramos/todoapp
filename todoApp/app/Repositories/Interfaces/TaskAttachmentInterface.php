<?php

namespace App\Repositories\Interfaces;

interface TaskAttachmentInterface
{
	public function createAttachmentFromTask($file);
	public function listAttachmentsFromTask($taskId);
	public function deleteAttachmentFromTask($fileId);
}