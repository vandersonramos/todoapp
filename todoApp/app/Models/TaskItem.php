<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TaskItem extends Model
{
    protected $table = 'list_item';
    protected $fillable = ['list_id', 'status', 'title', 'description','dueDate'];
}
