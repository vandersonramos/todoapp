# README #

todoApp is a simple API allowing people manage your todo list.


### Documentation ###

* (http://docs.todoapp8.apiary.io/)
* The host "http://dev.todoapp/api/" just is a example.

### How do I get set up? ###

* Create a directory (mkdir workspace)
* Go to the created directory (cd workspace/)
* Clone this repository (git clone git@bitbucket.org:vandersonramos/todoapp.git)
* Go to the app directory (cd todoapp/todoApp/)
* Run the composer command (composer install)
* Create a database on your localhost
* Create the .env file in the root app directory and configure with your database credentials
* Run the Migrations command to generate the table in the database (php artisan migrate)
* Run php server for start application (php artisan serve --port=8080)
* open your browser (http://127.0.0.1:8080/api/users/)

### Set folder permissions if necessary for application ###