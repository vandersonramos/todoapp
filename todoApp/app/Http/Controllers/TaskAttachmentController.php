<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateTaskAttachmentRequest;
use App\Repositories\Interfaces\TaskAttachmentInterface;
use Illuminate\Support\Facades\Response;
use Illuminate\Http\Request;

class TaskAttachmentController extends BaseController
{

	protected $attachmentRepository;

	public function __construct(TaskAttachmentInterface $attachmentRepository)
	{
		$this->attachmentRepository = $attachmentRepository;
	}

	public function createAttachmentFromTask(CreateTaskAttachmentRequest $request)
	{
		$file = $request->file('attachment');

		$data = $request->all();
		$data['list_id'] = $request->taskId;
		$data['filename'] = time().'.'.$file->getClientOriginalExtension();
		$data['mime'] = $file->getClientMimeType();
		$data['original_filename'] = $file->getClientOriginalName();
		$destinationPath = public_path('/files');
		$file->move($destinationPath, $data['filename']);

		$fileSaved = $this->attachmentRepository->createAttachmentFromTask($data);

		if ($fileSaved['saved']) {
			return $this->response_data('File uploaded successfully', $fileSaved['attachment'], 200);
		}

		return $this->response_data('Error upload file', $fileSaved['attachment'], 500);

	}

	public function listAttachmentsFromTask(Request $request)
	{
		$attachments = $this->attachmentRepository->listAttachmentsFromTask($request->taskId);

		if ($attachments['found']) {
			return $this->response_data('', $attachments['attachments'], 200);
		}
		return $this->response_data('Empty', $attachments['attachments'], 204);
	}

	public function deleteAttachmentFromTask(Request $request)
	{
		$attachment = $this->attachmentRepository->deleteAttachmentFromTask($request->attachmentId);

		if ($attachment['deleted']) {
			return $this->response_data('File deleted successfully', $attachment['attachment'], 200);
		}
		return $this->response_data('File not found', $attachment['attachment'], 404);
	}

}
