<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Requests\CreateTaskItemRequest;
use App\Http\Requests\UpdateTaskItemRequest;
use App\Repositories\Interfaces\TaskItemInterface;
use Illuminate\Support\Facades\Response;
use Illuminate\Http\Request;

class TaskItemController extends BaseController
{

	protected $itemRepository;

	public function __construct(TaskItemInterface $itemRepository)
	{
		$this->itemRepository = $itemRepository;
	}

	public function createItemFromTask(CreateTaskItemRequest $request)
	{
		$data = $request->all();
		$data['list_id'] = $request->taskId;
		$data['description'] = $request->description;
		$taskItem = $this->itemRepository->createItemFromTask($data, $request->taskId, $request->id);

		if (isset($taskItem['dueDateLimited'])) {
			return $this->response_data('You have 3 items with due date for today. This is not allowed', [], 442);
		}

		if ($taskItem['saved']) {
			return $this->response_data('Item created successfully', $taskItem['item'], 200);
		}

		return $this->response_data('Error saving item', $taskItem['item'], 500);
	}

	public function getItemFromTask(Request $request)
	{
		$taskItem = $this->itemRepository->getItemFromTask($request->id, $request->taskId, $request->itemId);
		if ($taskItem['found']) {
			return $this->response_data('', $taskItem['item'], 200);
		}
		return $this->response_data('Item not found', $taskItem['item'], 404);
	}

	public function deleteItemFromTask(Request $request)
	{
		$taskItem = $this->itemRepository->deleteItemFromTask($request->taskId, $request->itemId);
		if ($taskItem['deleted']) {
			return $this->response_data('Item deleted successfully', $taskItem['item'], 200);
		}
		return $this->response_data('Item not found', $taskItem['item'], 404);
	}

	public function getAllItemFromTask(Request $request)
	{
		$filters = ['status', 'dueDate'];

		if ($request->get('status')) {
			$filters['status'] = $request->get('status');
		}

		if ($request->get('dueDate')) {
			$filters['dueDate'] = $request->get('dueDate');
		}

		$taskItems = $this->itemRepository->listAllItemsFromTask($request->id, $request->taskId, $filters);
		if ($taskItems['found']) {
			return $this->response_data('', $taskItems['items'], 200);
		}
		return $this->response_data('Task is empty', $taskItems['items'], 200);
	}

	public function updateItemFromTask(UpdateTaskItemRequest $request)
	{
		$data = $request->all();
		$data['user_id'] = $request->id;
		$taskItem = $this->itemRepository->updateItemFromTask($data, $request->taskId, $request->itemId);

		if (isset($taskItem['dueDateLimited'])) {
			return $this->response_data('You have 3 items with due date for today. This is not allowed', [], 442);
		}

		if ($taskItem['updated']) {
			return $this->response_data('Item updated successfully', $taskItem['item'], 200);
		}

		return $this->response_data('Error update item', $taskItem['item'], 500);
	}

}
