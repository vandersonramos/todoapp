<?php

namespace App\Repositories\Interfaces;

interface TaskInterface
{
	public function createTaskFromUser($taskData);
	public function getTaskFromUser($userId, $taskId);
	public function updateTaskFromUser($taskData, $userId, $taskId);
	public function deleteTaskFromUser($userId, $taskId);
	public function listAllTasksFromUser($userId, $filters);
}